module ClickTracksHelper
  def stat_by(start_date, end_date)
    start_date ||= Time.current.beginning_of_month
    end_date ||= Time.current

    line_chart by_day_api_item_click_tracks_path(@item, start_date: start_date, end_date: end_date),
               basic_opts(start_date, end_date)
  end

  private

  def basic_opts(start_date, end_date)
    {
      discrete: true,
      download: true,
      legend: false,
      library: {
        yAxis: {
          title: {
            text: 'Count'
          }
        },
        credits: {
          enabled: false
        }
      }
    }
  end
end
